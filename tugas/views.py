from django.shortcuts import render,redirect
from .models import Message

# Create your views here.
def home(request):
    messages = Message.objects.all()
    return render(request,'pages/index.html',{'messages' : messages})

def confirm(request):
    request.session['name'] = request.POST['name']
    request.session['message'] = request.POST['message']
    
    return render(request, 'pages/confirmation.html',{})

def confirm_handler(request):
    if request.method == 'POST':
        name = request.session.get('name')
        message = request.session.get('message')
        new_message = Message.objects.create(name = name, message = message)
        new_message.save()
    return redirect('index')