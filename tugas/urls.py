from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='index'),
    path('confirm', views.confirm, name = 'confirm'),
    path('confirm_handler', views.confirm_handler, name= 'confirm_handler'),
]