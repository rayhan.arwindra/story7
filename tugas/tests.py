from django.test import TestCase
from selenium import webdriver
from .models import *
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys
import time
# Create your tests here.

class testDisplayForm(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()

        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.get('http://localhost:8000')
    
    def tearDown(self):
        self.browser.close()

    def test_title_is_displayed(self):

        #User sees title of page
        self.assertIn('Story7',self.browser.title)

    def test_page_content_displayed(self):
        
        #User sees welcome message
        assert 'Welcome, please enter a message:' in self.browser.page_source
        
    def test_post_form_goes_to_confirmation_page(self):
        
        #User sees form
        name_input = self.browser.find_element_by_name("name")
        message_input = self.browser.find_element_by_name("message")
        submit_button = self.browser.find_element_by_tag_name('button')

        #User fills and submits form
        name_input.send_keys('Name')
        message_input.send_keys('Status')
        submit_button.send_keys(Keys.RETURN)

        #Goes to confirmation page after submitting form
        self.assertEquals(self.browser.current_url, 'http://localhost:8000/confirm' )

    def test_confirm_goes_back_to_home_and_displays_message(self):

        #User sees form
        name_input = self.browser.find_element_by_name("name")
        message_input = self.browser.find_element_by_name("message")
        submit_button = self.browser.find_element_by_tag_name('button')

        #User fills and submits form
        name_input.send_keys('Name')
        message_input.send_keys('Status')
        submit_button.send_keys(Keys.RETURN)

        #User confirms to submit message
        confirm_button = self.browser.find_element_by_name('confirm')
        confirm_button.send_keys(Keys.RETURN)
        self.assertEqual(self.browser.current_url, 'http://localhost:8000/')

        #User sees message and name is displayed
        assert 'Name' in self.browser.page_source
        assert 'Status' in self.browser.page_source
    
    def test_cancel_goes_back_to_home(self):

         #User sees form
        name_input = self.browser.find_element_by_name("name")
        message_input = self.browser.find_element_by_name("message")
        submit_button = self.browser.find_element_by_tag_name('button')

        #User fills and submits form
        name_input.send_keys('NoName')
        message_input.send_keys('NoStatus')
        submit_button.send_keys(Keys.RETURN)

        #User confirms to submit message
        cancel_button = self.browser.find_element_by_name('cancel')
        cancel_button.send_keys(Keys.RETURN)
        self.assertEqual(self.browser.current_url, 'http://localhost:8000/')

        #User sees name and message is not displayed
        assert 'NoName' not in self.browser.page_source
        assert 'NoStatus' not in self.browser.page_source

    def test_button_change_status_color(self):
        
        #User sees button
        color_button = self.browser.find_element_by_name("colorButton")

        #User clicks button
        color_button.send_keys(Keys.RETURN)

        #Wait for transition
        time.sleep(2)

        #User sees that status cards change color
        card = self.browser.find_element_by_class_name("card")
        self.assertEquals(card.value_of_css_property("background-color"), "rgba(0, 123, 255, 1)")
        self.assertEquals(card.value_of_css_property("color"), "rgba(255, 255, 255, 1)")

        #User clicks button again to revert color
        color_button.send_keys(Keys.RETURN)

        #Wait for transition
        time.sleep(2)

        #User sees that status cards change back to original color
        card = self.browser.find_element_by_class_name("card")
        self.assertEquals(card.value_of_css_property("background-color"), "rgba(255, 255, 255, 1)")
        self.assertEquals(card.value_of_css_property("color"), "rgba(33, 37, 41, 1)")

