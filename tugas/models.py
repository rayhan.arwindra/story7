from django.db import models

class Message (models.Model):
    name = models.CharField(max_length = 200)
    message = models.CharField(max_length = 200)

    def __str__(self):
        return self.message
# Create your models here.